#include <stdio.h>
#include <math.h>

#include "utils.h"
#include "ZeroFuncao.h"

int main ()
{	
	Polinomio poli;
	int i;

	printf("Insira o grau do polinomio: \n");
	scanf("%d", &poli.grau);

	poli.p = malloc((poli.grau + 1)*(sizeof(double)));

	printf("Insira os coeficientes em ordem crescente. ");
	for (i = 0; i <= poli.grau; i++) 
		scanf("%lf", &poli.p[i]);
	

  return 0;
}

