#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "utils.h"
#include "ZeroFuncao.h"

double bisseccao (Polinomio p, double a, double b, double eps,
	       int *it, double *raiz)
{
	double Xm, Xmn, pXm, dpXm, pa, pb, dpa, dpb;
	calcPolinomio_rapido(p, a, pa, dpa);
	calcPolinomio_rapido(p, b, pb, dpb);

	if((pa*pb) >= 0)
		return -1;

	Xmn = (a+b)/2;

	while (abs((Xmn - Xm)/Xmn) > eps) {
		calcPolinomio_rapido(p, Xm, pXm, dpXm);
		if ((pa*fXm > 0)) {
			a = Xm;
			calcPolinomio_rapido(p, a, pa, dpa);
		}
		else {
			b = Xm;
			calcPolinomio_rapido(p, b, pb, dpb);
		}

		Xmn = (a+b)/2;
	}

	return Xmn;

}


double newtonRaphson (Polinomio p, double x0, double eps,
		   int *it, double *raiz)
{
	int k = 1;
	double x, erro, px0, dpx0;

	calcPolinomio_rapido(p, x0, dx0, dpx0);
	x = x0 - dx0/dpx0;

	while ((fabs(x-x0)/x) > eps && k < MAXIT) {
		x0 = x;
		k++;
		calcPolinomio_rapido(p, x0, dx0, dpx0);
		x = x0 - dx0/dpx0; 
	}
	raiz = x;
	it = k;
	if (k >= MAXIT)
		return -1;
	return 0;
}


double secante (Polinomio p, double x0, double x1, double eps,
	     int *it, double *raiz)
{

}


void calcPolinomio_rapido(Polinomio p, double x, double *px, double *dpx)
{

}


void calcPolinomio_lento(Polinomio p, double x, double *px, double *dpx)
{
	double i;

	for(i = 0; i <= p.grau; i++) {
		px += pow(x, i)*p.p[i];
	}
}

